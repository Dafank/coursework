﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace Work.UserControls
{
    public partial class UC_Indexes : UserControl
    {
        class Indexes
        {
            #region Create_Perfomance
            private PerformanceCounter cpuProcessorTime = new PerformanceCounter("Processor", "% Processor Time", "_Total");

            private PerformanceCounter cpuPrivilegedTime = new PerformanceCounter("Processor", "% Privileged Time", "_Total");

            private PerformanceCounter cpuInterruptTime = new PerformanceCounter("Processor", "% Interrupt Time", "_Total");

            private PerformanceCounter cpuDPCTime = new PerformanceCounter("Processor", "% DPC Time", "_Total");

            private PerformanceCounter memAvailable = new PerformanceCounter("Memory", "Available MBytes", null);

            private PerformanceCounter memCommited = new PerformanceCounter("Memory", "Committed Bytes", null);

            private PerformanceCounter memCommitLimit = new PerformanceCounter("Memory", "Commit Limit", null);

            private PerformanceCounter memCommitedPerc = new PerformanceCounter("Memory", "% Committed Bytes In Use", null);

            private PerformanceCounter memPollPaged = new PerformanceCounter("Memory", "Pool Paged Bytes", null);

            private PerformanceCounter memPollNonPaged = new PerformanceCounter("Memory", "Pool Nonpaged Bytes", null);

            private PerformanceCounter memCached = new PerformanceCounter("Memory", "Cache Bytes", null);

            private PerformanceCounter pageFile = new PerformanceCounter("Paging File", "% Usage", "_Total");

            private PerformanceCounter processorQueueLengh = new PerformanceCounter("System", "Processor Queue Length", null);

            private PerformanceCounter diskQueueLengh = new PerformanceCounter("PhysicalDisk", "Avg. Disk Queue Length", "_Total");

            private PerformanceCounter diskRead = new PerformanceCounter("PhysicalDisk", "Disk Read Bytes/sec", "_Total");

            private PerformanceCounter diskWrite = new PerformanceCounter("PhysicalDisk", "Disk Write Bytes/sec", "_Total");

            private PerformanceCounter diskAverageTimeRead = new PerformanceCounter("PhysicalDisk", "Avg. Disk sec/Read", "_Total");

            private PerformanceCounter diskAverageTimeWrite = new PerformanceCounter("PhysicalDisk", "Avg. Disk sec/Write", "_Total");

            private PerformanceCounter diskTime = new PerformanceCounter("PhysicalDisk", "% Disk Time", "_Total");

            private PerformanceCounter handleCountCounter = new PerformanceCounter("Process", "Handle Count", "_Total");

            private PerformanceCounter threadCount = new PerformanceCounter("Process", "Thread Count", "_Total");

            private PerformanceCounter contentSwitches = new PerformanceCounter("System", "Context Switches/sec", null);

            private PerformanceCounter systemCalls = new PerformanceCounter("System", "System Calls/sec", null);

            private PerformanceCounterCategory performanceNetCounterCategory;

            private PerformanceCounter[] trafficSentCounters;

            private PerformanceCounter[] trafficReceivedCounters;
            private string[] interfaces = null;

            public void initNetCounters()
            {
                performanceNetCounterCategory = new PerformanceCounterCategory("Network Interface");
                interfaces = performanceNetCounterCategory.GetInstanceNames();
                int length = interfaces.Length;
                if (length > 0)
                {
                    trafficSentCounters = new PerformanceCounter[length];
                    trafficReceivedCounters = new PerformanceCounter[length];               }

                for (int i = 0; i < length; i++)
                {
                    trafficReceivedCounters[i] = new PerformanceCounter("Network Interface", "Bytes Sent/sec", interfaces[i]);
                    trafficSentCounters[i] = new PerformanceCounter("Network Interface", "Bytes Sent/sec", interfaces[i]);
                }
            }
            #endregion

            public void getCPU_time(Label label)
            {
                float tmp = cpuProcessorTime.NextValue();
                var z= (float)(Math.Round((double)tmp, 1));
                label.Text = z.ToString()+"%";
            }

            public void getCPU_privileged(Label label)
            {
                float tmp = cpuPrivilegedTime.NextValue();
                var z = (float)(Math.Round((double)tmp, 1));
                label.Text = z.ToString() + "%";
            }

            public void getCPU_interrupt(Label label)
            {
                float tmp = cpuInterruptTime.NextValue();
                var z = (float)(Math.Round((double)tmp, 1));
                label.Text = z.ToString()+"%";
            }

            public void getCPU_deferred(Label label)
            {
                float tmp = cpuDPCTime.NextValue();
                var z = (float)(Math.Round((double)tmp, 1));
                label.Text = z.ToString() + "%";
            }

            public void getPage_file(Label label)
            {
                float tmp = pageFile.NextValue();
                var z = (float)(Math.Round((double)tmp, 1));
                label.Text = z.ToString() + " MB";
            }

            public void getProcessor_queue(Label label)
            {
                label.Text = processorQueueLengh.NextValue().ToString();
            }

            public void getMemory_available(Label label)
            {
                label.Text = memAvailable.NextValue().ToString()+" MB";
            }

            public void getMemory_commited(Label label)
            {
                label.Text = (memCommited.NextValue() / (1024 * 1024)).ToString()+" MB";
            }

            public void getMemory_commitLimit(Label label)
            {
                label.Text = (memCommitLimit.NextValue() / (1024 * 1024)).ToString()+" MB";
            }

            public void getMemory_commited_per(Label label)
            {
                float tmp = memCommitedPerc.NextValue();
                var z = (float)(Math.Round((double)tmp, 1));
                label.Text = z.ToString() + "%";
            }

            public void getMemPoolNonPaged(Label label)
            {
                float tmp = memPollNonPaged.NextValue() / (1024 * 1024);
                var z = (float)(Math.Round((double)tmp, 1));
                label.Text = z.ToString()+" MB";
            }

            public void getMemory_poolPaged(Label label)
            {
                float tmp = memPollPaged.NextValue() / (1024 * 1024);
                var z = (float)(Math.Round((double)tmp, 1));
                label.Text = z.ToString()+" MB";
            }

            public void getMemory_cache(Label label)
            {
                float tmp = memCached.NextValue() / (1024 * 1024);
                var z = (float)(Math.Round((double)tmp, 1));
                label.Text = z.ToString() + " MB";
            }

            public void getDisk_read(Label label)
            {
                float tmp = diskRead.NextValue() / 1024;
                var z = (float)(Math.Round((double)tmp, 1));
                label.Text = z.ToString()+" KB/s";
            }

            public void getDisk_write(Label label)
            {
                float tmp = diskWrite.NextValue() / 1024;
                var z = (float)(Math.Round((double)tmp, 1));
                label.Text = z.ToString()+" KB/s";
            }

            public void getDisk_averageTimeRead(Label label)
            {
                float tmp = diskAverageTimeRead.NextValue() * 1000;
                var z = (float)(Math.Round((double)tmp, 1));
                label.Text = z.ToString()+" ms";
            }

            public void getDisk_averageTimeWrite(Label label)
            {
                float tmp = diskAverageTimeWrite.NextValue() * 1000;
                var z = (float)(Math.Round((double)tmp, 1));
                label.Text = z.ToString()+" ms";
            }

            public void getDisk_time(Label label)
            {
                float tmp = diskTime.NextValue();
                var z = (float)(Math.Round((double)tmp, 1));
                label.Text = z.ToString()+"%";
            }

            public void getHandle_countCounter(Label label)
            {
                label.Text = handleCountCounter.NextValue().ToString();
            }

            public void getThread_count(Label label)
            {
                label.Text = threadCount.NextValue().ToString();
            }

            public void getContent_switches(Label label)
            {
                label.Text = Convert.ToString(Math.Ceiling(contentSwitches.NextValue()));
            }

            public void getSystem_calls(Label label)
            {
                label.Text = Convert.ToString(Math.Ceiling(systemCalls.NextValue()));
            }

            public void getCurrent_trafficSent(Label label)
            {
                int length = interfaces.Length;
                float sendSum = 0.0F;
                for (int i = 0; i < length; i++)
                {
                    sendSum += trafficSentCounters[i].NextValue();
                }
                float tmp = 8 * (sendSum / 1024);
                var z = (float)(Math.Round((double)tmp, 1));
                label.Text = z.ToString()+" kbps";
            }

            public void getCurrent_trafficReceived(Label label)
            {
                int length = interfaces.Length;
                float receiveSum = 0.0F;
                for (int i = 0; i < length; i++)
                {
                    receiveSum += trafficReceivedCounters[i].NextValue();
                }
                float tmp = 8 * (receiveSum / 1024);
                var z = (float)(Math.Round((double)tmp, 1));
                label.Text = z.ToString()+" kbps";
            }
        }

        Indexes Index;
        public UC_Indexes()
        {
            InitializeComponent();
            Index = new Indexes();
            Index.initNetCounters();
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Index.getCPU_time(label16);
            Index.getCPU_privileged(label17);
            Index.getCPU_interrupt(label18);
            Index.getCPU_deferred(label19);

            Index.getMemory_available(label20);
            Index.getMemory_commited(label21);
            Index.getMemory_commitLimit(label22);//static
            Index.getMemory_commited_per(label23);
            Index.getMemory_poolPaged(label24);
            Index.getMemPoolNonPaged(label33);
            Index.getMemory_cache(label25);

            Index.getPage_file(label26);
            Index.getProcessor_queue(label27);

            Index.getDisk_read(label28);
            Index.getDisk_write(label29);
            Index.getDisk_averageTimeRead(label30);
            Index.getDisk_averageTimeWrite(label41);
            Index.getDisk_time(label42);

            Index.getHandle_countCounter(label43);

            Index.getThread_count(label44);
            Index.getContent_switches(label45);
            Index.getSystem_calls(label46);
            Index.getCurrent_trafficSent(label48);
            Index.getCurrent_trafficReceived(label49);
        }

    }
}
