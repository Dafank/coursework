﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Work.UserControls
{
    public partial class UC_Stats : UserControl
    {
        public UC_Stats()
        {
            InitializeComponent();
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            float cpu = CPU.NextValue();
            float ram = RAM.NextValue();
            circularBar_CPU.Value = (int)cpu;
            circularBar_CPU.Text = string.Format("{0:0.00}%", cpu);


            circularBar_RAM.Value = (int)ram;
            circularBar_RAM.Text = string.Format("{0:0.00}%", ram);
        }
    }
}
