﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Work.UserControls;

namespace Work
{
    public partial class Form1 : Form
    {
        //TODO:
        /*
         * розібратися з всіми дивами другого контролера 
         * Панель збоку повина перепригувати назад на кнопку
         * Через таблицю доробити 3 Контролер
         * Просто щось от балди написати в 4 Контролер
         */
        UC_Stats stats;
        UC_Indexes index;
        UC_Tasks tasks;
        public Form1()
        {
            stats = new UC_Stats();
            index = new UC_Indexes();
            tasks = new UC_Tasks();
            InitializeComponent();
            AddControlPanel(stats);
        }

        bool dragging = false;
        int offset_x = 0;
        int offset_y = 0;
        private void button7_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (WindowState.ToString() == "Normal")
            {
                this.WindowState = FormWindowState.Maximized;
                (sender as Button).Image = Image.FromFile("../../minimaze.png");
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                (sender as Button).Image = Image.FromFile("../../maximaze.png");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void moveSidePanel(Control btn)
        {
            PanelSide.Top = btn.Top;
            PanelSide.Height = btn.Height;
        }

        private void AddControlPanel(Control c)
        {
            c.Dock = DockStyle.Fill;
            panelControl.Controls.Clear();
            panelControl.Controls.Add(c);
        }
        private void btnStats_Click(object sender, EventArgs e)
        {
            moveSidePanel(btnStats);
            AddControlPanel(stats);
        }

        private void btnIndex_Click(object sender, EventArgs e)
        {
            moveSidePanel(btnStats);
            AddControlPanel(index);
            moveSidePanel(btnIndex);
        }

        private void btnTasks_Click(object sender, EventArgs e)
        {
            AddControlPanel(tasks);
            moveSidePanel(btnTasks);
        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            moveSidePanel(btnAbout);
        }

        private void panel3_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;

            offset_x = Cursor.Position.X - this.Location.X;
            offset_y = Cursor.Position.Y - this.Location.Y;
        }

        private void panel3_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                this.Location = new Point(Cursor.Position.X - offset_x, Cursor.Position.Y - offset_y);
                this.WindowState = FormWindowState.Normal;
                button5.Image = Image.FromFile("../../maximaze.png");
                this.Update();
            }
        }

        private void panel3_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }

    }
}
