﻿namespace Work.UserControls
{
    partial class UC_Stats
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.circularBar_CPU = new CircularProgressBar.CircularProgressBar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.CPU = new System.Diagnostics.PerformanceCounter();
            this.RAM = new System.Diagnostics.PerformanceCounter();
            this.circularBar_RAM = new CircularProgressBar.CircularProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.CPU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RAM)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.label1.Location = new System.Drawing.Point(358, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(285, 33);
            this.label1.TabIndex = 0;
            this.label1.Text = "Main Computer Stats";
            // 
            // circularBar_CPU
            // 
            this.circularBar_CPU.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.circularBar_CPU.AnimationFunction = WinFormAnimation.KnownAnimationFunctions.Liner;
            this.circularBar_CPU.AnimationSpeed = 300;
            this.circularBar_CPU.BackColor = System.Drawing.Color.Transparent;
            this.circularBar_CPU.Font = new System.Drawing.Font("Century", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.circularBar_CPU.ForeColor = System.Drawing.Color.Navy;
            this.circularBar_CPU.InnerColor = System.Drawing.Color.White;
            this.circularBar_CPU.InnerMargin = 2;
            this.circularBar_CPU.InnerWidth = -1;
            this.circularBar_CPU.Location = new System.Drawing.Point(45, 156);
            this.circularBar_CPU.MarqueeAnimationSpeed = 0;
            this.circularBar_CPU.Name = "circularBar_CPU";
            this.circularBar_CPU.OuterColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.circularBar_CPU.OuterMargin = -25;
            this.circularBar_CPU.OuterWidth = 26;
            this.circularBar_CPU.ProgressColor = System.Drawing.Color.Blue;
            this.circularBar_CPU.ProgressWidth = 25;
            this.circularBar_CPU.SecondaryFont = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.circularBar_CPU.Size = new System.Drawing.Size(350, 350);
            this.circularBar_CPU.StartAngle = 270;
            this.circularBar_CPU.SubscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.circularBar_CPU.SubscriptMargin = new System.Windows.Forms.Padding(10, -35, 0, 0);
            this.circularBar_CPU.SubscriptText = "";
            this.circularBar_CPU.SuperscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.circularBar_CPU.SuperscriptMargin = new System.Windows.Forms.Padding(10, 35, 0, 0);
            this.circularBar_CPU.SuperscriptText = "";
            this.circularBar_CPU.TabIndex = 1;
            this.circularBar_CPU.TextMargin = new System.Windows.Forms.Padding(8, 8, 0, 0);
            this.circularBar_CPU.Value = 68;
            // 
            // timer1
            // 
            this.timer1.Interval = 300;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // CPU
            // 
            this.CPU.CategoryName = "Processor";
            this.CPU.CounterName = "% Processor Time";
            this.CPU.InstanceName = "_Total";
            // 
            // RAM
            // 
            this.RAM.CategoryName = "Memory";
            this.RAM.CounterName = "% Committed Bytes In Use";
            // 
            // circularBar_RAM
            // 
            this.circularBar_RAM.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.circularBar_RAM.AnimationFunction = WinFormAnimation.KnownAnimationFunctions.Liner;
            this.circularBar_RAM.AnimationSpeed = 500;
            this.circularBar_RAM.BackColor = System.Drawing.Color.Transparent;
            this.circularBar_RAM.Font = new System.Drawing.Font("Century", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.circularBar_RAM.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.circularBar_RAM.InnerColor = System.Drawing.Color.White;
            this.circularBar_RAM.InnerMargin = 2;
            this.circularBar_RAM.InnerWidth = -1;
            this.circularBar_RAM.Location = new System.Drawing.Point(605, 156);
            this.circularBar_RAM.MarqueeAnimationSpeed = 2000;
            this.circularBar_RAM.Name = "circularBar_RAM";
            this.circularBar_RAM.OuterColor = System.Drawing.Color.PeachPuff;
            this.circularBar_RAM.OuterMargin = -25;
            this.circularBar_RAM.OuterWidth = 26;
            this.circularBar_RAM.ProgressColor = System.Drawing.Color.Orange;
            this.circularBar_RAM.ProgressWidth = 25;
            this.circularBar_RAM.SecondaryFont = new System.Drawing.Font("Century", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.circularBar_RAM.Size = new System.Drawing.Size(350, 350);
            this.circularBar_RAM.StartAngle = 270;
            this.circularBar_RAM.SubscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.circularBar_RAM.SubscriptMargin = new System.Windows.Forms.Padding(10, -35, 0, 0);
            this.circularBar_RAM.SubscriptText = "";
            this.circularBar_RAM.SuperscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.circularBar_RAM.SuperscriptMargin = new System.Windows.Forms.Padding(10, 35, 0, 0);
            this.circularBar_RAM.SuperscriptText = "";
            this.circularBar_RAM.TabIndex = 3;
            this.circularBar_RAM.TextMargin = new System.Windows.Forms.Padding(8, 8, 0, 0);
            this.circularBar_RAM.Value = 68;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(186, 519);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 28);
            this.label2.TabIndex = 4;
            this.label2.Text = "CPU";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label3.Location = new System.Drawing.Point(749, 519);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 28);
            this.label3.TabIndex = 5;
            this.label3.Text = "RAM";
            // 
            // UC_Stats
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.circularBar_RAM);
            this.Controls.Add(this.circularBar_CPU);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Century Gothic", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MaximumSize = new System.Drawing.Size(1000, 600);
            this.Name = "UC_Stats";
            this.Size = new System.Drawing.Size(1000, 600);
            ((System.ComponentModel.ISupportInitialize)(this.CPU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RAM)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private CircularProgressBar.CircularProgressBar circularBar_CPU;
        private System.Windows.Forms.Timer timer1;
        private System.Diagnostics.PerformanceCounter CPU;
        private System.Diagnostics.PerformanceCounter RAM;
        private CircularProgressBar.CircularProgressBar circularBar_RAM;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}
