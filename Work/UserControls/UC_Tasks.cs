﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace Work.UserControls
{
    public partial class UC_Tasks : UserControl
    {
        //TODO
        /*
         * Доробити кілл та додавання процесу
         */
        DataTable dt = new DataTable();
        List<Process> processes;
        string sellectedname;
        public UC_Tasks()
        {
            InitializeComponent();
            processes = Process.GetProcesses().ToList();
            dt.Columns.Add("#", typeof(string));
            dt.Columns.Add("Id",typeof(string));
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("State", typeof(string));
            FillGrid();
            timer1.Start();
        }

        public void FillGrid()
        {
            dataGridView1.DataSource = null;
            dataGridView1.Refresh();
            int i = 1;
            foreach (var proc in processes)
            {
                dt.Rows.Add(i,proc.Id, proc.ProcessName, proc.Responding);
                i++;
            }
            dataGridView1.DataSource = dt;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            FillGrid();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (var proc in processes)
            {
                if (sellectedname == proc.ProcessName)
                {
                    proc.Kill();
                    break;
                }
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            DataGridViewRow selected = dataGridView1.Rows[index];
            sellectedname = selected.Cells[2].Value.ToString();
        }
    }
}
